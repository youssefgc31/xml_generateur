<%@ page language="java" contentType="text/html; charset=windows-1256"
	pageEncoding="windows-1256"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<html class="no-js lt-ie10" lang="en">
<html class="no-js" lang="en">
<head>
<meta charset="utf-8">

<title>Login page</title>

<meta name="description"
	content="ProUI is a Responsive Bootstrap Admin Template created by pixelcave and published on Themeforest.">
<meta name="author" content="pixelcave">
<meta name="robots" content="noindex, nofollow">

<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">



<!-- Stylesheets -->
<!-- Bootstrap is included in its original form, unaltered -->
<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- Related styles of various icon packs and plugins -->
<link rel="stylesheet" href="css/plugins.css">

<!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
<link rel="stylesheet" href="css/main.css">

<!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

<!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
<link rel="stylesheet" href="css/themes.css">
<!-- END Stylesheets -->

<!-- Modernizr (browser feature detection library) -->
<script src="js/vendor/modernizr.min.js"></script>
</head>
<body>

	<div id="page-wrapper">
		<div class="preloader themed-background">
			<h1 class="push-top-bottom text-light text-center">
				<strong>Pro</strong>UI
			</h1>
			<div class="inner">
				<h3 class="text-light visible-lt-ie10">
					<strong>Loading..</strong>
				</h3>
				<div class="preloader-spinner hidden-lt-ie10"></div>
			</div>
		</div>

		<div id="page-content">
			<div class="row">
				<div class="col-md-12">
					<div class="block">
						<div class="block-title">
							<h2>
								<strong>Entrez votre Login et Mot de passe</strong>
							</h2>
						</div>

						<form id="form-validation" action="controleur.php" method="post"
							class="form-horizontal form-bordered">
							<fieldset>

								<div class="form-group">
									<label class="col-md-3 control-label" for="example-hf-email">Email</label>
									<div class="col-md-9">
										<input type="text" name="users" value="${log.users}"
											class="form-control" placeholder="Enter Email.."> <span
											class="help-block">Please enter your email</span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="example-hf-password">Password</label>
									<div class="col-md-9">
										<input type="password" name="pass" value="${log.pass}"
											class="form-control" placeholder="Enter Password..">

										<span class="help-block">Please enter your password</span>
									</div>
								</div>
								<div class="form-group form-actions">
									<div class="col-md-9 col-md-offset-6">
										<button type="submit" class="btn btn-sm btn-primary">
											<i class="fa fa-user"></i> Login
										</button>
										<button type="reset" class="btn btn-sm btn-warning">
											<i class="fa fa-repeat"></i> Reset
										</button>
									</div>
								</div>
							</fieldset>
						</form>
 						<form id="form-validation" action="controleur.php" method="post" class="form-horizontal form-bordered">
                                        <fieldset>
                                            
                                            <div class="form-group">
                                            <label class="col-md-3 control-label" for="example-hf-email">Email</label>
                                            <div class="col-md-9">
                                                <input type="email"  name="users" value="${sign.users }" class="form-control" placeholder="Enter Email..">
                                                <span class="help-block">Please enter your email</span>
                                            </div>
                                           </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="example-hf-password">Password</label>
                                            <div class="col-md-9">
                                                <input type="password"  name="pass" value="${sign.pass}" class="form-control" placeholder="Enter Password..">
                                                <span class="help-block">Please enter your password</span>
                                            </div>
                                        </div>
                                                                                <div class="form-group">
                                            <label class="col-md-3 control-label" for="example-hf-password">Password</label>
                                            <div class="col-md-9">
                                                <input type="password"  name="pass" value="${sign.pass}" class="form-control" placeholder="Enter Password..">
                                                <span class="help-block">Confirm your password</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-actions">
                                            <div class="col-md-9 col-md-offset-6">
                                                <button type="submit" name="action" value="sign"class="btn btn-sm btn-primary"><i class="fa fa-user"></i> Sign Up</button>
                                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                                            </div>
                                        </div>
                                         </fieldset>   
                                        
                         </form>

						<script src="js/vendor/jquery.min.js"></script>
						<script src="js/vendor/bootstrap.min.js"></script>
						<script src="js/plugins.js"></script>
						<script src="js/app.js"></script>

						<!-- Load and execute javascript code used only in this page -->
						<script src="js/pages/formsValidation.js"></script>
						<script>
							$(function() {
								FormsValidation.init();
							});
						</script>
</body>
</html>